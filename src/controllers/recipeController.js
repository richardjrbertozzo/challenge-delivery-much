import getRecipesResult from '../services/recipeService';

export default class RecipeController {
  getRecipes(req, res) {
    const ingredientsParam = req.query.i;
    if (!ingredientsParam) return res.status(400).send('No ingredient informed');

    const ingredients = ingredientsParam.split(',');
    if (ingredients.length > 3) return res.status(400).send('More than 3 ingredients was support');

    return getRecipesResult(req.query)
      .then((recipes) => {
        const keywordsAndRecipesResponse = {
          keywords: ingredients,
          recipes,
        };

        res.status(200).send(keywordsAndRecipesResponse);
      })
      .catch(e => res.status(500).send(e.message));
  }
}
