import RecipeController from '../../controllers/recipeController';

export default (router) => {
  router.get('/recipes', (req, res) => {
    const Controller = new RecipeController();

    Controller.getRecipes(req, res);
  });
};
