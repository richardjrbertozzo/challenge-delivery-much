import { Router } from 'express';
import recipeRoutes from './api/recipesRoutes';

const router = Router();

recipeRoutes(router);

module.exports = router;
