import { get } from 'axios';

const getGiphy = async (term) => {
  try {
    const response = await get(`http://api.giphy.com/v1/gifs/search?q=${term}`, { headers: { api_key: process.env.GIPHY_API_KEY } });
    return response.data.data[0].images.original.url;
  } catch (error) {
    throw new Error('External api unavailable');
  }
};

export default getGiphy;
