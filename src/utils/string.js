const splitAndOrder = (string, caracter) => {
  const stringSplit = string.split(caracter).map(s => s.trim());

  return stringSplit.sort();
};

export default splitAndOrder;
